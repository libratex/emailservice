import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
	
public class GetPropertyValues {
	
		InputStream inputStream;
		String result="";
	 
		public String getPropValues() throws IOException {
	 
			try {
				Properties prop = new Properties();
				String propFileName = "config";
	 
				inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
	 
				if (inputStream != null) {
					prop.load(inputStream);
				} else {
					throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
				}
				
				String host = prop.getProperty("host");
				String port = prop.getProperty("port");
				String emailid = prop.getProperty("emailid");
				String password = prop.getProperty("password");
				
				result = host + "," + port + "," + emailid+","+password;
				
			} catch (Exception e) {
				System.out.println("Exception: " + e);
			} finally {
				inputStream.close();
			}
			return result;
		
	}
}

