import java.io.IOException;
import org.apache.commons.mail.*;

public class EmailService {
	
	public static void sendMail(String emailId,String data,String subject) throws EmailException{
		
		String[] recipients = {emailId};
		Email email = new SimpleEmail();
		
		GetPropertyValues properties = new GetPropertyValues();
		String result;
		
		try {
			result = properties.getPropValues();
			String host=result.split(",")[0];
			int port=Integer.parseInt(result.split(",")[1]);
			String emailid=result.split(",")[2];
			String password=result.split(",")[3];

			email.setStartTLSRequired(true);
			email.setStartTLSEnabled(true);

			email.setSmtpPort(port);
			email.setAuthenticator(new DefaultAuthenticator(emailid,password));
			email.setHostName(host);
			
			for (int i = 0; i < recipients.length; i++)
			{
			    email.addTo(recipients[i]);
			}
		
			email.setFrom(emailid);
			email.setSubject(subject);
			email.setMsg(data);
			email.send();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
